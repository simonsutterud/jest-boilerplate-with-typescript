import { noStrangers } from ".";

it("should return acquaintances", () => {
	expect(
		noStrangers("See Spot run. See Spot jump. Spot likes jumping. See Spot fly.")
	).toStrictEqual([["spot", "see"], []]);
});

it("should return acquaintances", () => {
	expect(
		noStrangers("See Spot run. See Spot jump. Spot likes jumping. See Spot fly. See See See")
	).toStrictEqual([["spot"], ["see"]]);
});

it("should return acquaintances", () => {
	expect(
		noStrangers(
			"See Spot run run run. See Spot jump jump jump. Spot likes jumping. See Spot fly. See See See. Spot Spot Spot"
		)
	).toStrictEqual([
		["run", "jump"],
		["see", "spot"],
	]);
});
