export function noStrangers(sentence: string) {
	const words = sentence.trim().replace(/\./g, "").toLowerCase().split(" ");

	const wordCount = {};
	let acquaintances = [];
	let friends = [];

	for (let i = 0; i < words.length; i++) {
		wordCount[words[i]] = wordCount[words[i]] ? wordCount[words[i]] + 1 : 1;

		if (wordCount[words[i]] === 3 && !acquaintances.includes(words[i])) {
			acquaintances.push(words[i]);
		}

		if (wordCount[words[i]] === 5 && !friends.includes(words[i])) {
			friends.push(words[i]);
			acquaintances = acquaintances.filter((ac) => ac !== words[i]);
		}
	}

	return [acquaintances, friends];
}
