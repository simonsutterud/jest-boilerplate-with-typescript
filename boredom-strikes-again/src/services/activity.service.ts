import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

const URL = 'https://www.boredapi.com/api/activity/';

@Injectable({
  providedIn: 'root',
})
export class ActivityService {
  constructor(private http: HttpClient) {}

  public getActivity() {
    return this.http.get(URL);
  }
}
