import { Component } from '@angular/core';
import { ActivityService } from 'src/services/activity.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'boredom-strikes-again';
  loading = false;
  activity = {} as any;

  constructor(private activityService: ActivityService) {}

  getActivity() {
    this.loading = true;
    this.activityService.getActivity().subscribe({
      next: (res) => {
        this.loading = false;
        this.activity = res;
      },
      error: (err) => {
        this.loading = false;
      },
    });
  }
}
