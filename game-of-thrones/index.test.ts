import { correctTitle } from ".";

it("should return the right format", () => {
	expect(correctTitle("jOn SnoW, kINg IN thE noRth")).toBe("Jon Snow, King in the North.");
});
it("should return the right format", () => {
	expect(correctTitle("sansa stark,lady of winterfell.")).toBe("Sansa Stark, Lady of Winterfell.");
});
it("should return the right format", () => {
	expect(correctTitle("TYRION LANNISTER, HAND OF THE QUEEN.")).toBe(
		"Tyrion Lannister, Hand of the Queen."
	);
});
it("should return the right format", () => {
	expect(correctTitle("TYRION-LANNISTER, HAND-OF-THE QUEEN.")).toBe(
		"Tyrion Lannister, Hand of the Queen."
	);
});
