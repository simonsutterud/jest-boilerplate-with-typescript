export function correctTitle(string: string) {
	const words = string
		.replace(/,/g, ", ")
		.replace(/  /g, " ")
		.replace(/-/g, " ")
		.toLowerCase()
		.split(" ");

	const lowerCaseWords = ["and", "the", "of", "in"];

	const capitalized = words.map((word) => {
		if (!lowerCaseWords.includes(word)) return word[0].toUpperCase() + word.slice(1);
		return word;
	});

	let newTitle = capitalized.join(" ");
	if (newTitle.at(-1) !== ".") newTitle += ".";

	return newTitle;
}
