/* 
Solving small problems 
- Unit Testing 
o Test Driven Development (TDD) 
o RED: Write Broken Tests 
o GREEN: Minimum Effort To make It work – Make the tests pass! 
o REFACTOR: Optimise 

You are counting points for a basketball game, given the amount of 2-pointers scored and 3-
pointers scored, find the final points for the team and return that value.
*/

export const points = (twoPointers: number, threePointers: number) =>
	twoPointers * 2 + threePointers * 3;
