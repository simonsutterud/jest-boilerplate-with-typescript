import { pigLatinSentence } from ".";

it("should return pig latin", () => {
	expect(pigLatinSentence("this is pig latin")).toBe("isthay isway igpay stinlay");
});

it("should return pig latin", () => {
	expect(pigLatinSentence("wall street journal")).toBe("allway eetstray ournaljay");
});

it("should return pig latin", () => {
	expect(pigLatinSentence("raise the bridge")).toBe("aiseray ethay idgebray");
});

it("should return pig latin", () => {
	expect(pigLatinSentence("all pigs oink")).toBe("allway igspay oinkway");
});
