export function pigLatinSentence(sentence) {
	let words = sentence.split(" ");

	for (let i = 0; i < words.length; i++) {
		let addedWay = false;

		if (isVowel(words[i][0])) {
			words[i] += "way";
			addedWay = true;
		}

		let letters = words[i].split("");
		let lettersToAdd = [];
		let hasSeenVowel;

		letters.forEach((letter) => {
			if (isVowel(letter)) {
				hasSeenVowel = true;
				return;
			}

			if (!hasSeenVowel) lettersToAdd.push(letter);
		});

		if (!addedWay) {
			let newWord = words[i].slice(0, lettersToAdd.length);
			newWord = words[i] += lettersToAdd + "ay";
			words[i] = newWord.substring(1);
		}
	}

	return words.join(" ");
}

function isVowel(letter) {
	return letter === "a" || letter === "e" || letter === "i" || letter === "o" || letter === "u";
}
