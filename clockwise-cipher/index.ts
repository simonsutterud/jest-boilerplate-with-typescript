export function clockwiseCipher(message: string) {
	let square = [
		[" ", " ", " ", " "],
		[" ", " ", " ", " "],
		[" ", " ", " ", " "],
		[" ", " ", " ", " "],
	];

	let row = 0;
	let column = 0;
	let shift = 0;
	let prevRow = 0;

	for (let i = 0; i < message.length; i++) {
		square[row][column] = message[i];

		row += 3;

		if (column > 3) column -= -4;
		if (row > 3) row -= -4;

		column = prevRow;
		prevRow = row;
	}

	console.log(square);
	return square;
}

// 0,0; 0,3; 3,3; 3,0
// 0,1; 1,3; 3,2; 2,0
// 0,2; 2,3; 3,1; 1,0
// 1,1; 1,2; 2,2; 2,1

/* for (let row = 0; row < 4; row++) {
		for (let column = 0; column < 4; column++) {
			if (column === 3 - shift) row = 3 - shift;
			square[row][column] = message[index];
			index += 3;
		}
		shift++;
	} */
