import { clockwiseCipher } from ".";

it("should return the encrypted string", () => {
	expect(clockwiseCipher("Mubashir Hassan")).toBe("Ms ussahr nHaaib");
});

it("should return the encrypted string", () => {
	expect(clockwiseCipher("Matt MacPherson")).toBe("M ParsoMc nhteat");
});

it("should return the encrypted string", () => {
	expect(clockwiseCipher("Edabit is amazing")).toBe("Eisadng  tm    i   zbia a");
});
