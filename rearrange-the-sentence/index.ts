export function rearrange(sentence) {
	const words = sentence.split(" ");
	const newSentence = [];

	for (let i = 0; i < words.length; i++) {
		const index = words[i].replace(/\D+/g, "");
		const wordWithoutNr = words[i].replace(/[^A-Za-z]+/g, "");

		newSentence[index] = wordWithoutNr;
	}

	return newSentence.join(" ").trim();
}
