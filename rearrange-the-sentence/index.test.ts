import { rearrange } from ".";

it("should return This is a Test", () => {
	expect(rearrange("is2 Thi1s T4est 3a")).toBe("This is a Test");
});

it("should return For the good of the people", () => {
	expect(rearrange("4of Fo1r pe6ople g3ood th5e the2")).toBe("For the good of the people");
});

it("should return JavaScript is so damn weird", () => {
	expect(rearrange("5weird i2s JavaScri1pt dam4n so3")).toBe("JavaScript is so damn weird");
});

it("should return an empty string", () => {
	expect(rearrange(" ")).toBe("");
});
