export function fiscalCode(person) {
	const formattedSur = handleSurname(person.surname);
	const formattedFName = handleFirstName(person.name);
	const formattedDOB = handleDOB(person.dob, person.gender);

	return formattedSur + formattedFName + formattedDOB;
}

function handleSurname(surname) {
	const surnameUpper = surname.toUpperCase();
	if (surnameUpper.length < 3) return surnameUpper + "X";

	let surnameConsonants = getConsonants(surnameUpper);
	if (surnameConsonants.length >= 3) return surnameConsonants.slice(0, 3);

	const surnameVowels = getVowels(surnameUpper);
	surnameConsonants += surnameVowels;
	return surnameConsonants.slice(0, 3);
}

function handleFirstName(firstName) {
	const firstNameUpper = firstName.toUpperCase();

	let firstNameConsonants = getConsonants(firstNameUpper);
	if (firstNameConsonants.length === 3) return firstNameConsonants;

	if (firstNameConsonants.length > 3)
		return firstNameConsonants[0] + firstNameConsonants[2] + firstNameConsonants[3];

	if (firstNameConsonants.length < 3) return firstNameConsonants + "X";

	const firstNameVowels = getVowels(firstNameUpper);
	firstNameConsonants += firstNameVowels;
	return firstNameConsonants.slice(0, 3);
}

function handleDOB(dateOfBirth, gender) {
	const [day, month, year] = dateOfBirth.split("/");

	const yearSlice = year.slice(-2);
	const monthLetter = months[month];
	let newDay;

	if (gender === "M") newDay = day < 10 ? 0 + day : day;
	if (gender === "F") newDay = Number(day) + 40;

	return yearSlice + monthLetter + newDay;
}

function getConsonants(str) {
	const consonantsRegEx = /[B-DF-HJ-NP-TV-Z]*/g;
	let consonants = str.match(consonantsRegEx).filter(Boolean).join("");
	return consonants;
}

function getVowels(str) {
	const vowelsRegEx = /[^B-DF-HJ-NP-TV-Z]*/g;
	const vowels = str.match(vowelsRegEx).filter(Boolean).join("");
	return vowels;
}

const months = {
	1: "A",
	2: "B",
	3: "C",
	4: "D",
	5: "E",
	6: "H",
	7: "L",
	8: "M",
	9: "P",
	10: "R",
	11: "S",
	12: "T",
};
