export function caesarCipher(str: string, shift: number) {
	const alphabet: string = "abcdefghijklmnopqrstuvwxyz";
	let encryptedString: string = "";

	for (let i = 0; i < str.length; i++) {
		let orgCharCode: number = str.charCodeAt(i);
		let newCharCode: number = orgCharCode + shift;

		// Handle positive "over-rotations"
		if (newCharCode > 122) newCharCode -= alphabet.length;
		if (orgCharCode <= 90 && newCharCode > 90) newCharCode -= alphabet.length;

		// Handle negative "over-rotations"
		if (newCharCode < 65) newCharCode += alphabet.length;
		if (orgCharCode >= 97 && newCharCode < 97) newCharCode += alphabet.length;

		// Spaces and "-" remain unchanged
		if (orgCharCode === 32 || orgCharCode === 45) newCharCode = orgCharCode;

		encryptedString += String.fromCharCode(newCharCode);
	}

	return encryptedString;
}
