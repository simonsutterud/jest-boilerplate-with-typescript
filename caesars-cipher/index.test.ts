import { caesarCipher } from ".";

it("should return the encrypted string", () => {
	expect(caesarCipher("middle-Outz", 2)).toBe("okffng-Qwvb");
});

it("should return the encrypted string", () => {
	expect(caesarCipher("Always-Look-on-the-Bright-Side-of-Life", 5)).toBe(
		"Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj"
	);
});

it("should return the encrypted string", () => {
	expect(caesarCipher("A friend in need is a friend indeed", 20)).toBe(
		"U zlcyhx ch hyyx cm u zlcyhx chxyyx"
	);
});

it("should return the encrypted string", () => {
	expect(caesarCipher("abcdefghijklmnopqrstuvwxyz", -2)).toBe("yzabcdefghijklmnopqrstuvwx");
});
