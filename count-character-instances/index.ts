export function charCount(char, string) {
	let occurrences = 0;
	Array.from(string).forEach((c) => {
		if (c === char) occurrences++;
	});

	return occurrences;
}

export function allCharCount(string) {
	const stringArr = Array.from(string);
	const uniqueChars = new Set([...stringArr]);

	const object = {};

	uniqueChars.forEach((char) => {
		object[char] = stringArr.filter((c) => c === char).length;
	});

	return object;
}
