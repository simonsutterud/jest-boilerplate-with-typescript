import { charCount, allCharCount } from ".";

it("should return 1", () => {
	expect(charCount("a", "edabit")).toBe(1);
});
it("should return 1", () => {
	expect(charCount("c", "Chamber of secrets")).toBe(1);
});
it("should return 0", () => {
	expect(charCount("B", "boxes are fun")).toBe(0);
});
it("should return 4", () => {
	expect(charCount("b", "big fat bubble")).toBe(4);
});
it("should return 0", () => {
	expect(charCount("e", "javascript is good")).toBe(0);
});
it("should return 2", () => {
	expect(charCount("!", "!easy!")).toBe(2);
});

it("should return an object with the # of occurrences for each char", () => {
	expect(allCharCount("hello")).toStrictEqual({ h: 1, e: 1, l: 2, o: 1 });
});

it("should return an object with the # of occurrences for each char", () => {
	expect(allCharCount("waddup")).toStrictEqual({ w: 1, a: 1, d: 2, u: 1, p: 1 });
});
